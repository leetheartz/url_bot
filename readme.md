# URL Shortener TG BOT
## Установка
* Клонируйте репозиторий
* Установите зависимости (`pip3 install pyrogram flask`)
* Переименуйте `config.sample.py` и `db.sample.json` в `config.py` и `db.json` соответственно
* Заполните файл `config.py` своими значениями
* Запустите bot.py и wsgi.py. 

## Gunicorn socket usage
Для использования сокетов gunicorn (например, если вы используете nginx), можно использовать команду из комментария wsgi.py

## Полезное
### Конфиги для SYSTEMD
#### `/etc/systemd/system/urls-web.service`
```
[Unit]
Description=URLS BOT WEB
After=network.target

[Service]
User=fuccsoc
WorkingDirectory=/home/fuccsoc/url_bot
ExecStart=gunicorn --workers 3 --bind unix:urls.sock wsgi:app

[Install]
WantedBy=multi-user.target
```
#### `/etc/systemd/system/urls-bot.service`
```
[Unit]
Description=URLS-BOT
After=network.target

[Service]
Restart=on-failure
RestartSec=1
User=fuccsoc
ExecStart=python3 bot.py
WorkingDirectory=/home/fuccsoc/url_bot/

[Install]
WantedBy=multi-user.target
```
### Конфиг для nginx
#### `/etc/nginx/sites-available/link.fuccsoc.com.conf` (reffer for symlink at `/etc/nginx/sites-enabled/link.fuccsoc.com`) 
```
server {
    server_name link.fuccsoc.com;

    location / {
        include proxy_params;
        proxy_pass http://unix:/home/fuccsoc/url_bot/urls.sock;
    }
}
```