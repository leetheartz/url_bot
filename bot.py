import random
from pyrogram import Client, filters, types
import json
import string

from pyrogram.methods.messages import edit_message_text
import config as c

from pyrogram.types.bots_and_keyboards.keyboard_button import KeyboardButton

bot = Client('bot-session', api_id=c.api_id, api_hash=c.api_hash, bot_token=c.bot_token)


@bot.on_message(filters.private)
async def handle_msg(client: Client, message: types.Message):
    if message.text:
        links = []
        if message.entities:
            for entity in message.entities:
                if entity.type == 'url':
                    link = message.text[ entity.offset : entity.offset + entity.length ]
                    if link[:4] != 'http':
                        link = 'http://' + link
                    links.append(link)
            if len(links) > 0:
                db = json.load(open('db.json', 'r'))
                for link in links:
                    id = ''.join([random.choice(string.ascii_lowercase) for i in range(0,5)])
                    db[id]=link
                    await message.reply(f'{c.base_url}{id}', reply_markup=types.InlineKeyboardMarkup([[types.InlineKeyboardButton("Delete", id)]]), disable_web_page_preview=True, reply_to_message_id=message.message_id)
                with open('db.json', 'w') as f:
                    json.dump(db, f)
            else:
                await message.reply('links not found')
        else:
            await message.reply("links not found")

@bot.on_callback_query()
async def handle_cb(client: Client, cb: types.CallbackQuery):
    db = json.load(open('db.json'))
    cbd = cb.data.split()
    if cbd[0] == 'confirm':
        link = cbd[1]
        href = db.get(link)
        if href:
            db.pop(link)
            with open('db.json', 'w') as f:
                json.dump(db, f) 
            await cb.answer("Success")
            await client.edit_message_text(cb.message.chat.id, cb.message.message_id, f"Link was deleted.\nID: {link}\nRef: {href}", disable_web_page_preview=True)
        else:
            await client.edit_message_text(cb.message.chat.id, cb.message.message_id, f"Link doesn't exist", disable_web_page_preview=True)
    elif cbd[0] == 'cancel':
        link = cbd[1]
        href = db.get(link)
        if href:
            await client.edit_message_text(cb.message.chat.id, cb.message.message_id, f'{c.base_url}{link}', reply_markup=types.InlineKeyboardMarkup([[types.InlineKeyboardButton("Delete", link)]]), disable_web_page_preview=True)
        else:
            await client.edit_message_text(cb.message.chat.id, cb.message.message_id, f"Link doesn't exist", disable_web_page_preview=True)
    else:
        link = cbd[0]
        href = db.get(link)
        if href:
            with open('db.json', 'w') as f:
                json.dump(db, f) 
            await cb.answer("Success")
            await client.edit_message_text(cb.message.chat.id, cb.message.message_id, f"Please, confirm.\nLink: {c.base_url}{link}\nRef: {href}", disable_web_page_preview=True, reply_markup=types.InlineKeyboardMarkup([[types.InlineKeyboardButton('Yes', f'confirm {link}'), types.InlineKeyboardButton('No', f'cancel {link}')]]))
        else:
            await client.edit_message_reply_markup(cb.message.chat.id, cb.message.message_id)
            await client.edit_message_text(cb.message.chat.id, cb.message.message_id, f"Link doesn't exist", disable_web_page_preview=True)
            await cb.answer("Error. Not found.")

bot.run()