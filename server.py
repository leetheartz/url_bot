from flask import Flask, redirect
import json

website = Flask(__name__)

@website.route("/<linkid>")
def handle(linkid):
    db = json.load(open('db.json'))
    l = db.get(linkid)
    if l:
        return redirect(l)
    else:
        return "404", 404