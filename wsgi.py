from server import website as app

if __name__ == "__main__":
    app.run()

# gunicorn --workers 3 --bind unix:urls.sock wsgi:app
